/*  Pin Definitions
 *  Digital Pin 8: Piezo speaker (connected to ground with 1M pulldown resistor)
 *  Digital Pin 4: Status LED (To visually indicate the state of the block)
 */

// Include Libraries
#include "PiezoListener.h"

// Pin Definitions
const int piezoSensor = A0; // Piezo sensor on pin 8.
const int statusLED = 4; // Status LED

// Tuning Constants
const int knockInterval = 150; // minimum required interval between 2 consecutive taps
const int maximumKnocks = 5; // Maximum number of knocks to listen for
const int knockComplete = 1200; // Longest time to wait for a knock before the taps are assumed complete

// Variables and objects
int *sensorReadings; //this is an array pointer which holds the intervals between knocks
int piezoSensorValue = 0; // default value of the piezo sensor
int numberOfRecordedIntervals=0; //number of individual taps in the current knock

void setup() 
{
  Serial.begin(9600);
  Serial.println("Program start");
}

void loop()
{
  piezoSensorValue = digitalRead(piezoSensor);
  
  // Listen for fisrt tap
  if (piezoSensorValue == HIGH)
  {
    // Knock has been detected
    Serial.println("Knock Detected");
    
    // make an object of the piezolistener type. This object contains all info about the current knock
    PiezoListener* p = new PiezoListener(statusLED, piezoSensor, knockInterval, knockComplete, maximumKnocks, true, 0, 100);
    
    // allow the object to listen for knocks
    sensorReadings = p->listenKnocks();// the sensorreadings now points to an array which contains all the time intervals between taps for the current knock
    numberOfRecordedIntervals = p->getSize();// gives the number of detected taps
    
    Serial.println("ARRAY THAT HAS BEEN RECORDED - PRINTING IN LOOP()");
    for(int i=0;i<numberOfRecordedIntervals;i++)
    {
        Serial.println(*(sensorReadings+i));
    }
    delete p;
  }
}
